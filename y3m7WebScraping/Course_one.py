'''
Created on 2016年6月29日

@author: equal
'''
from bs4 import BeautifulSoup
from urllib.request import urlopen

from bson.objectid import ObjectId
from pymongo.mongo_client import MongoClient



class CourseDetail(object):
    def __init__(self,coruseId):  
        self.coruseId = coruseId;
        
    def __eq__(self, other):  
        if isinstance(other, CourseDetail):  
            return ((self.coruseId == other.coruseId))  
        else:  
            return False  
    def __hash__(self):  
        return hash(str(self.coruseId));
      




def getCourseSet():
    client = MongoClient("118.165.26.26", 27017)
    db = client.course_db
#collection = db.COURSE_MASTER
    courses = db.restaurants.find();
    courseSet = set();
    for courseCount in range(0,courses.count(),1):
        #detailId = courses[courseCount]["_id"];
        courseId = courses[courseCount]["Label2"];
        courseDetail = CourseDetail(courseId);
        courseSet.add(courseDetail);
    return courseSet;


def getSpansSet():
    SOCID =  88456;
    EOCID = 95000;
    spansSet = set();
    for i in range(SOCID,EOCID,1):
        html = urlopen("http://tims.etraining.gov.tw/timsonline/index3.aspx?OCID="+str(i));
        bsObj = BeautifulSoup(html);
        tables = bsObj.findAll("table");
        table = tables[8];
        id = table.find("span",{"id":"Label2"}).text;
        print(id);
        if id is not None:
            spanDetail =  CourseDetail(id);
            spansSet.add(spanDetail);
    return spansSet;

if __name__ == '__main__':        
    try:
        #courseSet = getCourseSet();
        #print("courseSet:"+str(len(courseSet)));
        spansSet = getSpansSet();
        print("spansSet:"+str(len(spansSet)));
        #totalSet = courseSet + spansSet;
        #print("totalSet:"+str(len(totalSet)));
    except Exception as e:
        e.print_stack_trace();
    
# 
# 
# for span in spans:
#     spanId = parsetInt(span.find({"name":"Lable2"}));
#     span
    
    

#course = set(CourseDetail);
#courseDetail = CourseDetail();
#courseDetail.coruseId = 

#course.add(courseDetail);        