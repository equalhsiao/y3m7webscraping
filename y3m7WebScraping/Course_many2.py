'''

@author: equal
'''

from urllib.request import urlopen
from bs4 import BeautifulSoup
from bson.objectid import ObjectId
from pymongo.mongo_client import MongoClient



class CourseDetail(object):
    def __init__(self,coruseId):  
        self.coruseId = coruseId;
        
    def __eq__(self, other):  
        if isinstance(other, CourseDetail):  
            return ((self.coruseId == other.coruseId))  
        else:  
            return False  
    def __hash__(self):  
        return hash(str(self.coruseId));
      




def getCourseSet(courses):
    courseSet = set();
    for courseCount in range(0,courses.count(),1):
        #detailId = courses[courseCount]["_id"];
        courseId = courses[courseCount]["Label2"];
        courseDetail = CourseDetail(courseId);
        courseSet.add(courseDetail);
    return courseSet;


def getSpansSet(courseSet):
    SOCID =  88456;
    EOCID = 90000;
    #spansSet = set();
    for i in range(SOCID,EOCID,1):
        html = urlopen("http://tims.etraining.gov.tw/timsonline/index3.aspx?OCID="+str(i));
        bsObj = BeautifulSoup(html);
        tables = bsObj.findAll("table");
        table = tables[8];
        id = table.find("span",{"id":"Label2"}).text;
        print(id);
        if id is not None:
            spanDetail =  CourseDetail(id);
            courseSet.add(spanDetail);
    return courseSet;

if __name__ == '__main__':        
    client = MongoClient("y3m7.ddns.net", 27017)
    db = client.course_db
    courses = db.COURSE_MASTER.find();
        
    try:
        courseSet = getCourseSet(courses);
        print("courseSet:"+str(len(courseSet)));
        spansSet = getSpansSet(courseSet);
        print("spansSet:"+str(len(spansSet)));
        #totalSet = courseSet + spansSet;
        #print("totalSet:"+str(len(totalSet)));
    except Exception as e:
        courses.insert_one({"errorMessage":e.getMessage()});
        e.print_stack_trace();
    
# 
# 
# for span in spans:
#     spanId = parsetInt(span.find({"name":"Lable2"}));
#     span
    
    

#course = set(CourseDetail);
#courseDetail = CourseDetail();
#courseDetail.coruseId = 

#course.add(courseDetail);        